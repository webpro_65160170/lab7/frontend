import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import userService from '@/services/user'
import type { User } from '@/types/User'

export const useUserStore = defineStore('user', () => {
  //Data Function
  const loadingStore = useLoadingStore()
  const users = ref<User[]>([])

  async function getUser(id: number) {
    loadingStore.doload()
    const res = await userService.getUser(id)
    users.value = res.data
    loadingStore.finish()
  }
  async function getUsers() {
    loadingStore.doload()
    const res = await userService.getUsers()
    users.value = res.data
    loadingStore.finish()
  }
  async function saveUser(user: User) {
    loadingStore.doload()
    console.log('Post' + JSON.stringify(user))
    if (user.id < 0) {
      //Add new
      const res = await userService.addUser(user)
    } else {
      //Update
      console.log('Patch' + JSON.stringify(user))
      const res = await userService.updateUser(user)
    }
    getUsers()
    loadingStore.finish()
  }
  async function deleteUser(user: User) {
    loadingStore.doload()
    const res = await userService.delUser(user)
    await getUsers()
    loadingStore.finish()
  }

  return { users, getUsers, getUser, saveUser, deleteUser }
})
